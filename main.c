#include  "msp430x14x.h"
#include <stdio.h>
#include <math.h>

#define period 800  //设置总周期
#define c_distance  5


int lwheel_count = 0;   //左轮计数
int rwheel_count = 0;   //右轮计数
int car_m = 0;          //小车模式
int metal_num = 0;      //金属数
int step = 0;           //当前阶段
int whole_time = 0;     //整体运动到结束的时间
int flag =  0;  //中断标志位开关
int flag_2 = 0;
float metal_distance = 0.0; //铁皮距离起点的路程
unsigned int old_time = 0;
unsigned int old_time_2 = 0;
int flash_flag = 0;
unsigned int dog_time = 0;
unsigned int stop_time = 0;
unsigned int little_time = 0;

int metal_delay = 0;

static unsigned int light1;  //强1
static unsigned int light2;  //强2
static unsigned int light3;  //强3
static unsigned int light4;  //强4
static unsigned int light5;  //强5

/*超声1测距数据*/
int new_cap1 = 0;
int old_cap1 = 0;
int cap_diff1 = 0;
float distance1 = 0;


void Delay(int n)
{
    int i, j;

    for( i = 0; i < n; i++ )
    {
        for( j = 0; j < 100; j++ );
    }
}


/*时钟初始化*/
void clk_init()
{
    BCSCTL1 = 0X00; //打开XT2
    do
    {
        IFG1 &= ~OFIFG;
        for(int i = 0x20; i > 0; i--);
    }
    while((IFG1 & OFIFG) == OFIFG); //如果起震失败。。继续起震。直到成功为止
    BCSCTL2 = 0X00;
    BCSCTL2 |= SELM_2 | SELS; //mclk,sMCLK的时钟源为XT2，0分频。
}

/*IO口初始化*/
void io_init()
{
    P1DIR = BIT2 + BIT3; //P1.2 P1.3口用于输出控制H桥的使能端
    P1SEL = BIT2 + BIT3; //P1.2 P1.3口开第二功能 ，TA1和TA2

    P2DIR = BIT7;   //开启声光功能，输出
    P2SEL = 0x00; //不需要开第二功能

    P3DIR = 0x00;  //用于循迹传感器
    P3SEL = 0x00;  //用于循迹传感器

    P4DIR = BIT1; //P4.1输出PWM波，激发超声波传感器
    P4SEL = BIT1 + BIT2 ; // P4.1输出PWM波，P4.2进行捕获

    P5DIR = 0x0f; //低四位全开输出用于控制车轮
    P5SEL = 0x00; //不要开第二功能
    P5OUT = 0x05; //小车先正转启动

    P6DIR = 0x00; //输入6路信号
    P6SEL = 0x1f; //P6.0~P6.4为ADC输入，强6为开关量，不需要ADC

}

/*设置小车速度*/
void speed_setting(int T , int speedL, int speedR)
{
    TACCR0 = T;
    TACCR1 = speedL;  //设置左轮速度
    TACCR2 = speedR;  //设置右轮速度
}


/*设置小车的行走模式*/
int car_mode(int mode)
{
    switch(mode)
    {
    case 1:
        P5OUT = 0x05; //正转
        speed_setting(period, 280, 280);//减少
        return 1;
    //正常速度前进
    case 2:
        P5OUT = 0x05; //数轮子
        speed_setting(period, 0, 350);
        return 2;//滑行
    case 3:
        speed_setting(period, 0, 0);
        P5OUT = 0x00;  //锁死停车
        return 3;//刹车停车
    case 4:
        P5OUT = 0x05; //正转
        speed_setting(period, 200, 720);
        return 4;//减速左转,左轮慢，右轮快
    case 5:
        P5OUT = 0x05; //正转
        speed_setting(period, 600, 250);
        return 5;//减速右转
    case 6:
        P5OUT = 0x06;
        speed_setting(period, 400 , 400 );
        return 6;//原地转顺（逆） 时针,可用于原地旋转90度，声波2、3找与障碍平行的位置
    case 7:
        P5OUT = 0x09;
        speed_setting(period, 400 , 400);
        return 7;//原地转逆（顺）时针,可用于原地旋转90度，声波2、3找与障碍平行的位置
    case 8:
        P5OUT = 0x05; //正转
        speed_setting(period, 0 , 760);
        return 8;  //使劲左转
    case 9:
        P5OUT = 0x05; //正转
        speed_setting(period, 580 , 0);  //使劲右转
        return 9;
    }
    return 10;
}


void find_black_line_init(void)
{
    /*检测金属*/
    //P2IES = BIT0;    //P2.0口下降沿触发,检测到金属，P2.4和P2.5检测黑线
    P2OUT = 0x00;
    P2IES = 0x00;      //上升沿中断
    //P2IES  =  BIT0; //切换中断为下降沿使能
    P2IFG =  0x00;      //清中断标志位
    P2IE  =  BIT0;   //开中断允许

    P1IES = 0x00;  //红5 和 红6 上升沿触发
    P1IE  = BIT7; //使能红1和红2的中断
    P1IFG = 0x00;     //清中断标志位

    /*开始第一阶段*/
    step  =  1;
}

/*初始化定时器A，用于输出控制H桥使能的PWM波*/
void timerA_init()
{
    TACCTL1 = OUTMOD_7;
    TACCTL2 = OUTMOD_7;
    car_m  = car_mode(1);     //启动
    TACTL   = TASSEL_2 +  MC_1 + TACLR;  //SMCLK,增计数模式。清空TAR，0分频
}



#pragma vector=PORT1_VECTOR
__interrupt void Port1()
{
    static int mode_count = 0;
    if( P1IFG & BIT7 )
    {
        P1IFG &= ~BIT7; //清除中断标志
        if(mode_count == 0)
        {
            //P1IE = 0x00;  //关掉P1的中断,消抖
            //flag_2 = 1;     //进入后打开flag
            //P2OUT =  BIT7; //光电闪烁
            //P2IES  = 0x00; //切换为上升沿使能
            P1IES =  BIT7; //切换中断为下降沿使能
            old_time_2 = dog_time;
        }
        else
        {
            if(dog_time - old_time_2 >= 4)
            {
                dog_time = 0; //先清零，避免溢出出错
                rwheel_count++;    //辐条数加1
            }
            P1IES  = 0x00; //切换为上升沿使能
            //P1IE = 0x00;  //关掉P1的中断
            //flag_2 = 1;     //也进入定时器中断延时消抖

            //P2OUT = 0x00; //停止闪烁和鸣叫
            //P2IES =  BIT0; //切换中断为下降沿使能
            //metal_distance =  (wheel_out + wheel_in) / 2.0 * 2.2; //走过的距离，厘米
        }
        mode_count  =  1 - mode_count;  //上升沿为值为0，下降沿值为1
    }
}

/*P2中断*/
#pragma vector=PORT2_VECTOR
__interrupt void Port2()
{
    static int mode_flash = 0;
    /*金属片计数，进入时触发*/
    if( P2IFG & BIT0 )
    {
        //处理P1IN.0中断
        P2IFG &= ~BIT0; //清除中断标志
        //以下填充用户代码
        /*上升沿探测到金属进入中断，光电响应。切换为下降沿触发*/
        if(mode_flash == 0)
        {
            P2IE = 0x00;  //关掉P2的金属中断,消抖
            flag = 1;     //进入后打开flag
            //P2OUT =  BIT7; //光电闪烁
            //P2IES  = 0x00; //切换为上升沿使能
            P2IES =  BIT0; //切换中断为下降沿使能
            //wheel_in = lwheel_count;  //进入时的轮子数
            old_time = little_time;
        }
        else
        {
            /*大于0.1秒，因为毛刺很多，所以间隔一般不会大于0.1秒*/
            if(little_time - old_time >= 10 )
            {
                metal_num++;    //铁片数加1
                flash_flag = 1;
            }
            flag = 1;     //也进入定时器中断延时消抖

            P2IES  = 0x00; //切换为上升沿使能
            //P2IE = 0x00;  //关掉P2的金属中断
            if(metal_num >= 4)
            {

                car_m = car_mode(3);  //停车
                step++;              //进入下一阶段
                stop_time = whole_time; //记下当前走过的时间
                flag = 0;     //关掉定时器间隔重启中断消抖的开关
                P2IE = 0x00;    //把P2口的中断全部关闭
            }

            //P2OUT = 0x00; //停止闪烁和鸣叫
            //P2IES =  BIT0; //切换中断为下降沿使能


            //wheel_out = lwheel_count; //出去时的轮子数
            //metal_distance =  (wheel_out + wheel_in) / 2.0 * 2.2; //走过的距离，厘米
        }
        mode_flash  =  1 - mode_flash;  //上升沿为值为0，下降沿值为1
    }
}



/*数轮子数直行对应距离的程序，虽然感觉并没有什么用*/
void go_distance(float distance)
{
    int origin_wheel;
    origin_wheel = rwheel_count ; //理想状态

    while(((rwheel_count - origin_wheel) * 5.5) < distance); //对应当下走过的厘米数 race未达到distance前不停往前走
    car_m = car_mode(3);
    Delay(10);          //停车
}

void car_rotate(int mode_r , int distance)
{
    car_m =  car_mode(3);
    Delay(10); //停车
    if(mode_r == -1)
    {
        car_m  =  car_mode(6); //逆时针旋转模式开启
    }
    else
        car_m  =  car_mode(7); //顺时针旋转模式开启
    go_distance(distance); //宏定义旋转所需要走过的路程
    //函数调用完记得调整车的状态
}

void wtd_init()
{
    WDTCTL = WDT_MDLY_0_5; //0.5ms,注意区别
    IE1 |= WDTIE; //允许WDT中断
}



/*每0.5毫秒进入依次此中断*/
#pragma vector=WDT_VECTOR
__interrupt void WatchDog()
{

    static int times = 0;  //计数到2000次，表示1秒
    static int flag_count = 0; //中断延时计数
    //static int flag_count_2 = 0;
    if(times == 2000) //0.5 * 2 * 1000
    {
        times  = 0;    //重置
        whole_time++;  //计数1秒
    }
    /*0.01秒计数器*/
    if(times >= 20)
    {
        if(times % 20 == 0)
        {
            little_time++;
        }
    }
    if(flag)
    {
        flag_count++;  //计数
        if (flag_count == 20) // 2 * 0.5 * 10 = 10ms
        {
            P2IE  = BIT0 ;  //打开金属检测中断
            flag_count  = 0; //10ms后清零
            flag = 0;  //清标志位
        }
    }
    /*
    if(flag_2)
    {
        flag_count_2++;  //计数
        if (flag_count == 20) // 2 * 0.5 * 10 = 10ms
        {
            P1IE  =  BIT7 ;  //打开检测中断
            flag_count_2  = 0; //10ms后清零
            flag_2 = 0;  //清标志位
        }
    }
    */
    times++;  //不停加
    dog_time++; //0.5ms
}

void sound_init(void)
{
    TBCCR0 = 32768 - 1;       //装入PWM 周期值

    //    TBCCTL0 = CCIE;           //使能中断，计数到CCR0跳中断

    TBCCTL1 = OUTMOD_7;       //设CCR1 输出单元为复位/置位输出模式
    TBCCR1 = 350;             //装入CCR1 PWM的占空值
    //先开超1再说
    TBCCTL2 = CM_3 + CCIS_0 + SCS + CAP + CCIE; //  双边沿触发 + 选择4.2口引入 + 同步捕获 +捕获模式 + 中断使能

    TBCTL = TBSSEL_2 + MC_1 + TBCLR + ID_3 ; //时钟源选择smclk, 定时器B,增计数模式,清TimerB,8分频,关溢出中断

}

/*超声中断*/
#pragma vector=TIMERB1_VECTOR
__interrupt void TimerB1(void)
{
    static int mode_1 = 0;
    switch(TBIV)
    {
    /*超1处理程序*/
    case 4:
    {
        if(mode_1 == 0)
        {
            old_cap1 = TBCCR2;
        }
        else
        {
            new_cap1 = TBCCR2;
            cap_diff1 = new_cap1 - old_cap1;
            /*用这种中介变量的方法反而更好，5800将近2m，滤过过大和负数的数据*/
            if(cap_diff1 < 10000 && cap_diff1 > 20)
            {
                distance1 = (cap_diff1 / 1000000.0 ) * 340 / 2;
            }
        }
        mode_1 = 1 - mode_1;
    }
    break;
    default:
        return;
    }
}

void adc_init(void)
{
    ADC12CTL0 = ADC12ON + MSC + SHT0_8; //开ADC12模块+采样信号由SHI仅首次触发+采集定时器分频系数n=64,
    ADC12CTL1 = SHP + CONSEQ_3; // 使用采样定时器输出作采集/转换信号SAMPCON
    // 重复序列采样模式
    ADC12MCTL0 = INCH_0; // 参考电压ref+=AVcc, 输入通道选择为 A0
    ADC12MCTL1 = INCH_1; // 参考电压ref+=AVcc, 输入通道选择为 A1
    ADC12MCTL2 = INCH_2; // 参考电压ref+=AVcc, 输入通道选择为 A2
    ADC12MCTL3 = INCH_3 ; // 参考电压ref+=AVcc, 输入通道选择为 A3
    ADC12MCTL4 = INCH_4 + EOS;
    ADC12IE    = BIT4; // A3通道开中断ADC12IFG.5
    ADC12CTL0 |= ENC; // 允许转换
    ADC12CTL0 |= ADC12SC; // 启动转换
}

/*1~5左到右*/
#pragma vector=ADC_VECTOR //ADC 中断服务程序
__interrupt void ADC12ISR (void)
{
    light1 = 4095 - ADC12MEM0;
    light2 = 4095 - ADC12MEM1;
    light3 = 4095 - ADC12MEM2;
    light4 = 4095 - ADC12MEM3;
    light5 = 4095 - ADC12MEM4;
}

void main()
{
    clk_init(); //时钟初始化
    wtd_init(); //改成看门狗
    io_init();
    timerA_init();
    _EINT();
    find_black_line_init();  //写一段开一段的设置，再关一段的设置，再开下一段的配置，以此类推
    while(1)
    {
        while(step == 1)
        {
            if(flash_flag )//直行时亮响
            {
                P2OUT = BIT7;
                Delay(100);
                P2OUT = 0x00;
                Delay(100);
                flash_flag = 0;
            }
            if(  P3IN & BIT0 )
            {
                Delay(2);
                car_m = car_mode(9);
                while( P3IN & BIT0 )  //最右
                {
                    if(flash_flag )//使劲右拐时亮响
                    {
                        P2OUT = BIT7;
                        Delay(100);
                        P2OUT = 0x00;
                        Delay(100);
                        flash_flag = 0;
                    }
                }
                Delay(2);
                car_m = car_mode(1);
            }

            else if(P3IN & BIT3) //最左
            {
                Delay(2);
                car_m = car_mode(8);
                while(P3IN & BIT3)
                {
                    if(flash_flag )//使劲左拐时亮响
                    {
                        P2OUT = BIT7;
                        Delay(100);
                        P2OUT = 0x00;
                        Delay(100);
                        flash_flag = 0;
                    }
                }
                car_m = car_mode(1);
            }


            else if(P3IN & BIT1)
            {
                Delay(2);
                if(P3IN & BIT1)
                {
                    car_m = car_mode(3); //先停车
                    Delay(20);
                    car_m = car_mode(5);
                    while(P3IN & BIT1) //中右
                    {
                        if(flash_flag )//小右拐时亮响
                        {
                            P2OUT = BIT7;
                            Delay(100);
                            P2OUT = 0x00;
                            Delay(100);
                            flash_flag = 0;
                        }
                    }
                    car_m = car_mode(1);
                }
            }
            else if( P3IN & BIT2)  //中左
            {
                Delay(2);
                if(P3IN & BIT2)
                {
                    car_m = car_mode(3); //先停车
                    Delay(20);
                }
                car_m = car_mode(4);
                while(P3IN & BIT2)
                {
                    if(flash_flag )////小左拐时亮响
                    {
                        P2OUT = BIT7;
                        Delay(100);
                        P2OUT = 0x00;
                        Delay(100);
                        flash_flag = 0;
                    }
                }
                car_m = car_mode(1);
            }
        }
        //step = 2; //这个是为了直接测试第二阶段，把第一阶段注释掉了，然后直接从这里开始用的
        sound_init();
        while(step == 2)
        {
            while(whole_time - stop_time <= 5 )
            {
                car_m = car_mode(3);  //停车
                /*不断闪烁*/
                P2OUT = BIT7;
                Delay(100);
                P2OUT = 0x00;
                Delay(100);
            }
            /*旋转至找到墙壁位置*/
            while( !(distance1 <= 0.88 && distance1 >= 0.33 ) )
            {
                car_rotate(-1, 22); //逆时针反转一圈的距离
                Delay(10000);
            }
            car_m = car_mode(3); //先停车
            Delay(10000);
            /*直行到墙前面停车*/
            while( distance1 >= 0.3)  //放大一些，因为有惯性
            {
                car_m = car_mode(1);  //直行
            }
            car_m = car_mode(3); //再停车
            Delay(10000);
            car_rotate(-1, 32); //逆时针转大约90度
            Delay(10000);
            /*直行直至遇到强光*/
            while( P6IN & BIT5 ) /*检测到强光置零，平时输出高电平*/
            {
                car_m  = car_mode(1); //直行到检测到强光为止
            }
            /*旋转至光强方向*/
            car_m = car_mode(3);
            Delay(10000);
            car_rotate(1, 40); //顺时针转大约90度多
            step++;         //进入下一阶段
        }
        adc_init();  //初始化ADC模块
        static int light_dif = 0;
        while(step == 3)
        {
            /*停车进入下一阶段,调整*/
            if(distance1 <= 0.5)
            {
                car_m = car_mode(3);
                step++;    //进入下一阶段
            }
            /*找光源，自准直*/
            light_dif = (light1 + light2) - (light4 + light5); //存入当时的值
            /*左边比右边光强*/
            if( light_dif >= 300 )
            {
                car_m  =  car_mode(8);  //减速左转
            }
            else if(  light_dif  <= -300)
            {
                car_m = car_mode(9);  //减速右转
            }
            else
            {
                car_m = car_mode(1); //直行
            }
        }
        /*后来因为各种原因就没有做完了
        上面的阶段三这么跑的效果是向光源处前进，我们曾经试着打着闪光灯遛狗玩
        其实想设置为停止也很简单
        可以超声测到灯泡的距离
        还可以用光强直接判断，因为没有实测过，就直接写在下面的注释了，放到上面的函数段中即可
        */
        //if( light3 > close_value ) { car_m = car_mode(3); step++; } //中间传感器测到足够光强，在灯泡前停车，也可以用于灭火机器人等
        while(step == 4);
    }
}
